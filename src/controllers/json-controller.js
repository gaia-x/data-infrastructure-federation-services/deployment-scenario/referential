const etag = require('etag');
const {JsonModel} = require('../models/json-model');
const {logger} = require('../utils/utils');

const saveJsonData = async (req, res) => {
    try {
        const {object_name, version} = req.params;
        const content = req.body;

        const result = await JsonModel.updateOne(
            {object_name, version},
            {$set: {content, timestamp: new Date()}},
            {upsert: true}
        );

        if (result.upsertedCount > 0) {
            res.status(201).send({
              message: 'A new document has been created.'
            });
        } else if (result.modifiedCount > 0) {
            res.status(200).send({
              message: 'An existing document has been updated.'
            });
        }
    } catch (error) {
        logger.error('Error while updating or inserting document', {error});
        res.status(500).send({error: 'An error has occurred'});
    }
};

const getJsonData = async (req, res) => {
    try {
        const {object_name, version} = req.params;
        const data = await JsonModel.findOne({object_name, version});
        
        if (!data) {
            res.status(404).send({error: 'Object not found'});
        } else {
            const generatedEtag = etag(JSON.stringify(data.content));
            if (req.headers['if-none-match'] === generatedEtag) {
                return res.status(304).end();
            }

            res.setHeader('ETag', generatedEtag);
            res.status(200).send({content: data.content});
        }
    } catch (error) {
        logger.error('Error while while getting document', {error});
        res.status(500).send({error: 'An error has occurred'});
    }
}

module.exports = {saveJsonData, getJsonData};
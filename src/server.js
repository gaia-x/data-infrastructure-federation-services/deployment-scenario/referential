const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const compression = require('compression');
const {connectDb} = require('./config/db');
const {initReferential} = require('./config/load-files');

const app = express();
const PORT = 4003;

app.use(compression());
app.use(bodyParser.json());
app.use(cors());

async function startServer() {
    await connectDb();
    await initReferential();
    app.use('/', require('./routes/json-routes'));

    app.listen(PORT, () => {
        console.log(`server started at port ${PORT}`);
    });
}

startServer()
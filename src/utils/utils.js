const winston = require("winston");

exports.checkApiKey = (req, res, next) => {  
    if (req.headers?.['x-api-key'] === process.env.REFERENTIAL_API_KEY) {
        next();
    } else {
        return res.status(403).send({error: 'Unauthorized: API key is invalid'});
    }
}

exports.logger = winston.createLogger({
    format: winston.format.combine(
        winston.format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
        }),
        winston.format.errors({ stack: true }),
        winston.format.printf(info => {
            const log = {
                timestamp: info.timestamp,
                level: info.level.toUpperCase(),
                message: info.message
            };

            if (info.error) {
                log.error = info.error;
            }

            return JSON.stringify(log);
        })
    ),
    transports: [new winston.transports.Console({})],
});
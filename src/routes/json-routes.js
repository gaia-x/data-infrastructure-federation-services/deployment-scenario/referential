const express = require('express');
const {saveJsonData, getJsonData} = require('../controllers/json-controller');
const {checkApiKey} = require('../utils/utils');
const router = express.Router();

router.put('/:version/:object_name', checkApiKey, saveJsonData);
router.get('/:version/:object_name', getJsonData);

module.exports = router;
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const JsonModel = mongoose.model('JsonData', new Schema({
    object_name: String,
    version: String,
    content: Schema.Types.Mixed,
    ts: {type: Date, default: Date.now}
    }, {versionKey: false} 
));

module.exports = {JsonModel};
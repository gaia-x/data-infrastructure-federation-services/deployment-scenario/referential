const mongoose = require('mongoose');
const {logger} = require('../utils/utils');

const connectDb = async () => {
    try {
        let MONGO_URL = (typeof process.env.MONGO_INITDB_ROOT_USERNAME !== 'undefined') ? `mongodb://${process.env.MONGO_INITDB_ROOT_USERNAME}:${process.env.MONGO_INITDB_ROOT_PASSWORD}@${process.env.MONGO_SERVICE_HOST}:${process.env.MONGO_SERVICE_PORT}/Referential?authSource=admin` : 'mongodb://localhost:27020/Referential';
        await mongoose.connect(MONGO_URL);
        logger.info('db connection succeeded.');
    } catch(error) {
        logger.error('connection failed ', error);
        process.exit(1);
    }
}

module.exports = {connectDb};
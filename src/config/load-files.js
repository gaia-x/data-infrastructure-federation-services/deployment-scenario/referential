const fs = require('fs');
const path = require('path');
const {JsonModel} = require('../models/json-model');
const {logger} = require('../utils/utils');
const directoryWithConfigfiles = "bootstrap"
 
async function UpdateFilesVersion (dir, version) {
    let fullPath = path.join(dir, version);
    let files = fs.readdirSync(fullPath, {withFileTypes: true});
    for (const file of files) {
        if (!file.isDirectory()) {
            let stat = fs.statSync ( path.join(fullPath, file.name))
            try {
                await JsonModel.updateOne(
                    {object_name: file.name, version},
                    {$set: {content: fs.readFileSync(path.join(fullPath, file.name)).toString(), ts: stat.ctime}},
                    {upsert: true}
                );
            } catch(error) {
                logger.error(`Error while updating or inserting file(${file.name} in ${version}) from bootstrap directory`, {error});
            }
        }
    }
}
 
async function loadFiles (dir) {
    let directories = fs.readdirSync(dir, { withFileTypes: true })
    for (const directory of directories) {
        if (directory.isDirectory()) {
            UpdateFilesVersion (dir, directory.name) 
        }
    }
}
 
exports.initReferential = async () => {
    let dir = path.join(__dirname, '..', '..', directoryWithConfigfiles);
    await loadFiles (dir);
}